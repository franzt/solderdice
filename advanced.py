

# Bibliotheken laden
import socket
import utime as time
# Bibliotheken laden

import network

import time
import ntptime


# Client-Betrieb
wlan = network.WLAN(network.STA_IF)

# WLAN-Interface aktivieren
wlan.active(True)

# WLAN-Verbindung herstellen
wlan.connect('CCCMZ', 'd68a13ccea')

# WLAN-Verbindungsstatus prüfen
import utime as time
print('Warten auf WLAN-Verbindung')
while not wlan.isconnected() and wlan.status() >= 0:
    time.sleep(1)
print('WLAN-Verbindung hergestellt / Status:', wlan.status())
print(wlan.ifconfig())



# HTML
html = """<!doctype html><html lang="en"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><link rel="shortcut icon" href="data:"><title>Raspberry Pi Pico</title></head><body><h1 align="center">Raspberry Pi Pico W</h1><p align="center">Verbindung mit %s</p></body></html>"""

# HTTP-Server starten
print('Server starten')
addr = socket.getaddrinfo('0.0.0.0', 80)[0][-1]
server = socket.socket()
server.bind(addr)
server.listen(1)
print('Server hört auf', addr)

print()
print('Beenden mit STRG + C')
print()

# Auf eingehende Verbindungen hören
if False:
    try:
        conn, addr = server.accept()
        print('HTTP-Request von Client', addr)
        request = conn.recv(1024)
        # HTTP-Request anzeigen
        print('Request:', request)
        # HTTP-Response senden
        response = html % str(addr)
        conn.send('HTTP/1.0 200 OK\r\nContent-type: text/html\r\n\r\n')
        conn.send(response)
        conn.close()
        print('HTTP-Response gesendet')
        print()
    except OSError as e:
        break
    except (KeyboardInterrupt):
        break

try: conn.close()
except NameError: pass
server.close()
print('Server beendet')



time.localtime()
ntptime.settime()
time.localtime()
