from machine import Pin
import time
import random


#1   7
#2 4 6
#3   5o

#01    40
#02 08 20
#04    10

ONE = 0x08
TWO = 0x11
THRE= 0x19
FOUR= 0x55
FIVE= 0x5D
SIX=  0x77

numbers=[ONE, TWO, THRE, FOUR, FIVE, SIX]


# create an output pin on pin #0
p1 = Pin(16, Pin.OUT)
p2 = Pin(17, Pin.OUT)
p3 = Pin(18, Pin.OUT)
p4 = Pin(19, Pin.OUT)
p5 = Pin(20, Pin.OUT)
p6 = Pin(21, Pin.OUT)
p7 = Pin(22, Pin.OUT)
b  = Pin(26, Pin.IN, Pin.PULL_UP)

def iterLed(i):
    if i==1:
        setLed(ONE)
    if i==2:
        setLed(TWO)
    if i==3:
        setLed(THRE)
    if i==4:
        setLed(FOUR)
    if i==5:
        setLed(FIVE)
    if i==6:
        setLed(SIX)
        
        
def setLed(i):
    print("====")
    print(i)
    if i%2==1:
        p1.high()
        print(1)
    else:
        p1.low()
    
    if i%4//2==1:
        p2.high()
        print(2)
    else:
        p2.low()    
    
    if i%8//4==1 :
        p3.high()
        print(3)
    else:
        p3.low()    
    
    if i%16//8==1 :
        p4.high()
        print(4)
    else:
        p4.low()    
    
    if i%32//16==1:
        p5.high()
        print(5)
    else:
        p5.low()    
    
    
    if i%64//32==1:
        p6.high()
        print(6)
    else:
        p6.low()    

    if i%128//64==1:
        p7.high()
        print(7)
    else:
        p7.low()
        




#setLed(ONE)
#time.sleep(1)
#setLed(TWO)
#time.sleep(1)
#setLed(THRE)
#time.sleep(1)
#setLed(FOUR)
#time.sleep(1)
#setLed(FIVE)
#time.sleep(1)
#setLed(SIX)
#time.sleep(1)
iterLed(random.randint(1, 6))

while True:
    logic_state=b.value() #storing the value of the push button state in the variable logic_state
    if logic_state==False:  #if state of button is logic 1    
        iterLed(random.randint(1, 6))

