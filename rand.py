from machine import Pin
import time
import random
import machine
button = machine.Pin(26, machine.Pin.IN, machine.Pin.PULL_UP)

#1   7
#2 4 6
#3   5

#01    40
#02 08 20
#04    10

ONE = 0x08
TWO = 0x11
THRE= 0x19
FOUR= 0x55
FIVE= 0x5D
SIX=  0x77

numbers=[ONE, TWO, THRE, FOUR, FIVE, SIX]


# create an output pin on pin #0
p1 = Pin(16, Pin.OUT)
p2 = Pin(17, Pin.OUT)
p3 = Pin(18, Pin.OUT)
p4 = Pin(19, Pin.OUT)
p5 = Pin(20, Pin.OUT)
p6 = Pin(21, Pin.OUT)
p7 = Pin(22, Pin.OUT)

    
def setLed(i):
    print("====")
    print(i)
    if i%2==1:
        p1.high()
        print(1)
    else:
        p1.low()
    
    if i%4//2==1:
        p2.high()
        print(2)
    else:
        p2.low()    
    
    if i%8//4==1 :
        p3.high()
        print(3)
    else:
        p3.low()    
    
    if i%16//8==1 :
        p4.high()
        print(4)
    else:
        p4.low()    
    
    if i%32//16==1:
        p5.high()
        print(5)
    else:
        p5.low()    
    
    
    if i%64//32==1:
        p6.high()
        print(6)
    else:
        p6.low()    

    if i%128//64==1:
        p7.high()
        print(7)
    else:
        p7.low()    


while True:
    if not button.value():
        print('Button pressed!')
        i=random.randint(0,5)
        print('Button pressed!'+str(i))
        setLed(numbers[i])
        time.sleep(.1)